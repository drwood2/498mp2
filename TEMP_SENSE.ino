int tempPin=A0;
int tempSupply=A1;
int POWER=2;
int GND = 3;

int temp = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
    
    pinMode(tempPin,INPUT);
    pinMode(POWER,OUTPUT);
    pinMode(GND,OUTPUT);
     digitalWrite(POWER,1);
  digitalWrite(GND,0);
    Serial.println( "Arduino started sending bytes via XBee" );
    
}


void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(POWER,1);
  digitalWrite(GND,0);
  analogWrite(tempSupply,300);
  temp=analogRead(tempPin);
  //power saving by disabling temp sensor when not in use
  digitalWrite(POWER,0);
  Serial.print("\nTemperature data is:");
  Serial.println(temp);
  Serial.print('\n');
  //digitalWrite(POWER,0);
  delay(5000);

}
