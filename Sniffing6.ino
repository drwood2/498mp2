// by Ray Burnette 20161013 compiled on Linux 16.3 using Arduino 1.6.12

#include <ESP8266WiFi.h>
#include "./functions.h"

#define disable 0
#define enable  1

#define D0 16
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13

// uint8_t channel = 1;
unsigned int channel = 1;

unsigned int timer = 0;

void setup() {
  Serial.begin(57600);
  Serial.printf("\n\nSDK version:%s\n\r", system_get_sdk_version());
  Serial.println(F("ESP8266 mini-sniff by Ray Burnette http://www.hackster.io/rayburne/projects"));
  Serial.println(F("Type:   /-------MAC------/-----WiFi Access Point SSID-----/  /----MAC---/  Chnl  RSSI"));

  wifi_set_opmode(STATION_MODE);            // Promiscuous works only with station mode
  wifi_set_channel(channel);
  wifi_promiscuous_enable(disable);
  wifi_set_promiscuous_rx_cb(promisc_cb);   // Set up promiscuous callback
  wifi_promiscuous_enable(enable);

  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
}

void loop() {
  channel = 1;
  wifi_set_channel(channel);
  
  while (true) {
    nothing_new++;                          // Array is not finite, check bounds and adjust if required
    if (nothing_new > 200) {
      nothing_new = 0;
      channel++;
      if (channel == 15) break;             // Only scan channels 1 to 14
      wifi_set_channel(channel);
    }
    delay(1);  // critical processing timeslice for NONOS SDK! No delay(0) yield()
    // Press keyboard ENTER in console with NL active to repaint the screen
    if ((Serial.available() > 0) && (Serial.read() == '\n')) {
      Serial.println("\n-------------------------------------------------------------------------------------\n");
      for (int u = 0; u < clients_known_count; u++) print_client(clients_known[u]);
      for (int u = 0; u < aps_known_count; u++) print_beacon(aps_known[u]);
      Serial.println("\n-------------------------------------------------------------------------------------\n");
    }

    
    timer = millis();
    int count = 0;
    for (int u = 0; u < clients_known_count; u++){
      if(timer < clients_known[u].timer+6000){
        //Serial.println(clients_known[u].timer);
        //Serial.println("unit's timer\n");
        count++;
      }
    }
    for (int u = 0; u < aps_known_count; u++){
      if(timer < aps_known[u].timer+6000){
        count++;
      }   
    }
    //Serial.println("time is\n");
    //Serial.println(timer);
    Serial.println("we have ");
    Serial.println(count);
    Serial.println(" unique devices\n");
    digitalWrite(D0,count%2);
    digitalWrite(D1,(count>>1)%2);
    digitalWrite(D2,(count>>2)%2);
    digitalWrite(D3,(count>>3)%2);
    digitalWrite(D4,(count>>4)%2);
    digitalWrite(D5,(count>>5)%2);
    digitalWrite(D6,(count>>6)%2);
    digitalWrite(D7,(count>>7)%2);

    int sum = digitalRead(D0)+2*digitalRead(D1)+4*digitalRead(D2)+8*digitalRead(D3)+16*digitalRead(D4)+32*digitalRead(D5)+64*digitalRead(D6)+128*digitalRead(D7);
    Serial.printf("%d",sum);
    Serial.printf(" is the sum\n");   

    
  }
}
