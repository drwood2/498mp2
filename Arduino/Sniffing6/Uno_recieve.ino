
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
#define D8 8
#define D9 9
  int counter = 0;

#include <XBee.h>
#include <SoftwareSerial.h>

XBee xbee = XBee(); /* Create an object named xbee(any name of your choice) of the class XBee */
ZBRxIoSampleResponse ioSamples = ZBRxIoSampleResponse(); 
/* Create an object named ioSamples(any name of your choice) of the class ZBRxIoSampleResponse */
void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  //Code taken from https://www.electronicwings.com/arduino/xbee-s2-zigbee-interfacing-with-arduino-uno

  xbee.setSerial(Serial); /* Define serial communication to be used for communication with xbee */
  /* In this case, software serial is used. You could use hardware serial as well by writing "Serial" in place of "xbee_ss" */
  /* For UNO, software serialis required so that we can use hardware serial for debugging and verification */
  /* If using a board like Mega, you can use Serial, Serial1, etc. for the same, and there will be no need for software serial */
  pinMode(D2, INPUT);
  pinMode(D3, INPUT);
  pinMode(D4, INPUT);
  pinMode(D5, INPUT);
  pinMode(D6, INPUT);
  pinMode(D7, INPUT);
  pinMode(D8, INPUT);
  pinMode(D9, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  counter = digitalRead(D2)+2*digitalRead(D3)+4*digitalRead(D4)+8*digitalRead(D5)+16*digitalRead(D6)+32*digitalRead(D7)+64*digitalRead(D8)+128*digitalRead(D9);
  Serial.println(counter);
  delay(1000);
}
