import datetime
#from digi.xbee.devices import XBeeDevice
import flask 
import redis
import atexit
import serial

device = serial.Serial("/dev/ttyUSB1", 9600)
device.close()
device.open()

f = open("/home/pi/dev/air_sensor/temperature_data_log.csv", 'a')
# f.write('air_quality,mac_addresses\n')

while True:
  msg = device.readline()
  if msg is not None:
    print("xbee test msg", msg)
    break
  else:
    print("no data")


app = flask.Flask(__name__)
data_log = []

def close_device():
  f.close()
  device.close()


@app.route('/')
def index():
  #return "xbee test msg:" +  device.read_data().data

  return """
	<!doctype html>
	<div style="display:table;">
	<img src="/static/floorplan.png" style="float:left;width:50%;">
	<img src="/static/gmap.png" style="float:left;width:50%;">
	</div><br>
	Air quality: <span id="airQuality"></span>
	<br> Temperature: <span id="temperature"></span>
	<br> Mac Addresses: <span id="macAddresses"></span>
	<br> Data updated time: <span id="time"></span>
	<script> function sse() {
		console.log("sse");
		var source = new EventSource('/stream');
		var airQuality = document.getElementById('airQuality');
		var macAddresses = document.getElementById('macAddresses');
                var tempElem = document.getElementById('temperature');
		var timeElem = document.getElementById('time');
		source.onmessage = function(e) {
			console.log(e);
			dataObj = JSON.parse(e.data);
			console.log(dataObj);
			airQuality.textContent = dataObj.airQuality;
			tempElem.textContent = dataObj.temperature;
			timeElem.textContent =  dataObj.time;
			macAddresses.textContent = dataObj.macAddresses;
		};
	}
	sse();
	</script>
""" 

def event_stream():
  air_quality = None
  air_quality_prefix = "Air data is:"
  mac_addresses = None
  mac_addresses_prefix = "MAC Addresses is:"
  temperature = None
  temperature_prefix = "Temperature data is:"

  while True:
    xbee_msg = device.readline()
    if xbee_msg is not None:
      print("received: ", xbee_msg)
      try:
        xbee_msg = xbee_msg.decode().strip()
        if xbee_msg.startswith(air_quality_prefix):
          air_quality = xbee_msg.replace(air_quality_prefix, "")
        elif xbee_msg.startswith(mac_addresses_prefix):
          mac_addresses = xbee_msg.replace(mac_addresses_prefix, "")
        elif xbee_msg.startswith(temperature_prefix):
          temperature = int(xbee_msg.replace(temperature_prefix, ""))-785
      except:
        continue
    if temperature is not None and (air_quality is not None and mac_addresses is not None):
      timestamp = datetime.datetime.now()
      f.write(str(temperature) + ',' + air_quality + ',' + mac_addresses + ',' + str(timestamp) + '\n')
      time = timestamp.ctime()
      yield 'data: {{ "temperature": "{temp}", "airQuality": "{air_quality}", "macAddresses": "{mac_addresses}", "time": "{time}" }} \n\n'.format(temp=temperature, air_quality=air_quality, mac_addresses=mac_addresses, time=time)
      break

@app.route('/stream')
def stream():
  return flask.Response(event_stream(), mimetype="text/event-stream")


if __name__ == '__main__':
  atexit.register(close_device)
  app.run(debug=True, host='0.0.0.0')

#while True:
#  xbee_msg = device.read_data()
#  if xbee_msg is not None:
    #print(xbee_msg.data)
#    print(xbee_msg.data.decode(), end ="")
